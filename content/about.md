+++
title = "Acerca del HackLab Uni Popular en La Ferroviaria"
description = "Espacio interdisciplinar de aprendizaje cooperativo y cacharreo tecnológico utilizando software libre. "
date = "2020-09-26"
aliases = ["about-us", "about-hugo", "contact"]
author = "HackLab"
+++

En esta página web encontrarás la información del HackLab Uni Popular en La Ferroviaria