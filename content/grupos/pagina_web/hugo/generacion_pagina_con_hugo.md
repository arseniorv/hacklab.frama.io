+++
title = "Generación de una página web con Hugo"
description = "¿Te interesa cómo crear una nueva página web con Hugo?"
date = 2022-06-20T16:07:44+02:00
weight = 5
chapter = false
pre = "<b> </b>"
tags = [
    "Web estática",
    "Hugo",
    "Git"
]
toc = true
+++

# ¿Cómo crear una nueva página web con Hugo?

Pasos:

1. [Instalar Hugo](https://gohugo.io/getting-started/installing/)

    Si estás en Debian o Ubuntu puedes correr:

    ```bash
    sudo apt-get install hugo
    ```

    Si usas Mac asumiendo que tienes [Homebrew](https://brew.sh/):

    ```bash
    brew install hugo
    ```

    Si estás con Windows y usas [Chocolatey](https://chocolatey.org/):

    ```
    choco install hugo -confirm
    ```

2. Elegimos un [tema](https://themes.gohugo.io/). Por ejemplo, el tema de esta web es [ReLearn](https://github.com/McShelby/hugo-theme-relearn). Además, al final de esta página se exponen algunos temas recomendados.


3. Creamos un nuevo proyecto con:

    ```
    hugo new site <name-project>
    ```

4. Nos desplazamos a la carpeta e inicializamos el repositorio.

    ```
    git init
    ```

5. Clonamos el tema elegido en la carpeta `themes` o añadimos el submódulo del repositorio. Por sencillez, se recomienda la opción de clonar el tema.

    * Mediante https:

    ```
    git clone https://github.com/McShelby/hugo-theme-relearn.git themes/relearn
    ```

    * Mediante SSH:

    ```
    git clone git@github.com:McShelby/hugo-theme-relearn.git themes/relearn
    ```

    O añadimos el submódulo, e:

    ```
    git submodule add https://github.com/McShelby/hugo-theme-relearn.git themes/relearn
    ```

6. Editamos el fichero de configuración `config.toml` y añadimos:

    ```
    baseURL = "http://localhost:1313/"
    theme = "relearn"
    ```

7. Servimos la página web con el siguiente comando y está disponible en la ruta [http://localhost:1313/](http://localhost:1313/).

    ```
    hugo server
    ```

8. Crear nuevo contenido en Hugo. Por ejemplo en el tema Learn, crearemos un nuevo capítulo con el siguiente comando:

    ```
    hugo new --kind chapter hugo/_index.md
    ```

    O si queremos crear una nueva entrada, escribimos:

    ```
    hugo new hugo/quick_start.md
    ```

8. Publicamos el proyecto en GitLab.

# Algunos temas de Hugo recomendados

* Documentación técnica:
    - [ReLearn](https://github.com/McShelby/hugo-theme-relearn)
    - [Docsy](https://www.docsy.dev/)

* Organización/Empresa:
    - [Fiction](https://scenic-sea.cloudvent.net/)
    - [Hugo Scroll](https://janraasch.github.io/hugo-scroll/)

* E-commerce:
    - [Fur](https://adept-lemongrass.cloudvent.net/)
