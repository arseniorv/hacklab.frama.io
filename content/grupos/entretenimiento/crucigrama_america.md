+++
title = "Crucigramas"
description = "Muestra de un proyecto"
date = 2022-11-06T18:07:44+02:00
weight = 15
chapter = false
pre = "<b> </b>"
tags = [
]
toc = false
+++

Este crucigrama es un proyecto personal de una persona miembro de Hacklab.

{{< rawhtml >}}
<html>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta charset="UTF-8">
  <style>
    *,
    ::after,
    ::before {
      box-sizing: inherit;
    }

    body {
      color: #000000;
      font-family: "Helvetica Neue", "Helvetica", "Arial", sans-serif;
      font-size: 16px;
      /* font-size: 1.6em; */
      font-weight: 300;
      letter-spacing: .01em;
      line-height: 1.6;
      margin: 0;
      padding: 1em;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      font-weight: 300;
      letter-spacing: -.1rem;
      margin-bottom: 1.0rem;
      margin-top: 0;
    }

    /* h3 {
      font-size: 2.2rem;
      letter-spacing: -.08rem;
      line-height: 1.35;
    } */


    #crossword-view {
      font-size: 1.2em;
    }

    #crossword-resp {
      min-height: 50px;
    }

    #crossword-resp * {
      margin: 0;
      text-align: center;
    }

    .crossword-board {
      margin: 0 auto;
      text-align: center;
      float: left;
      margin: 0 1em;
      min-width: 550px;
    }

    .crossword-board .board {
      display: inline-block;
      /* border: 1px solid black; */
      border-radius: 4px;
    }

    .crossword-board .line::before,
    .crossword-board .line::after {
      content: "";
      display: block;
      clear: both;
    }


    .crossword-board .cell {
      width: 2em;
      height: 2em;
      text-align: center;
      float: left;
      border: 1px solid black;
    }

    .crossword-board .line:nth-of-type(2) .cell {
      border-top-width: 2px;
    }

    .crossword-board .line:last-of-type .cell {
      border-bottom-width: 2px;
    }

    .crossword-board .cell:nth-of-type(2) {
      border-left-width: 2px;
    }

    .crossword-board .cell:nth-of-type(2).numv {
      border-left-width: 2px;
    }

    .crossword-board .cell:last-of-type {
      width: 2em;
      height: 2em;
      text-align: center;
      float: left;
      border: 1px solid black;
      border-right-width: 2px;
    }

    .crossword-board .cell.blank {
      margin-right: 2px;
      border: 2px solid transparent;
    }

    .crossword-board .cell.numh {
      margin-right: 2px;
      border-width: 1px 2px;
      line-height: 1.8em;
      cursor: pointer;
    }

    .crossword-board .cell.numv {
      margin-bottom: 2px;
      border-width: 2px 1px;
      line-height: 1.8em;
      cursor: pointer;
    }

    .crossword-board .numv:last-of-type {
      border-right-width: 2px;
    }

    .crossword-board .cell .block {
      display: block;
      background-color: black;
      height: 100%;
    }

    .crossword-board .cell .cell-input {
      -webkit-appearance: none;
      background-color: white;
      border: 0;
      border-radius: 0;
      box-shadow: none;
      box-sizing: inherit;
      height: 100%;
      width: 100%;
      padding: 0;
      margin: 0;
      font-size: inherit;
      text-align: center;
    }

    .crossword-board .cell .cell-input.corrected {
      background-color: pink;
    }

    .crossword-defs .definitions h3,
    .crossword-defs .definitions p {
      text-align: left;
      margin: 0 0 1ex 0;
    }

    .crossword-defs .definitions .abbr {
      color: green;
      position: relative;
    }

    .crossword-defs .definitions .abbr::before {
      /* position: relative; */
      background-color: white;
      color: #293133;
      border: 1px solid #293133;
      border-radius: 7px;
      position: absolute;
      top: 1.5em;
      left: 0px;
      padding: .3ex 1.5ex;
      z-index: 10;
      line-height: 1.2em;
    }

    .crossword-defs .definitions .abbr:hover::before {
      content: attr(data-title);
      display: block;
      /* top: 20; */
    }

    .crossword_controles h4 {
      display: inline-block;
      margin: 0 0 1ex 0;
    }

    .crossword_controles button {
      font-size: 1.2rem;
      border: 1px solid goldenrod;
      border-radius: 7px;
      background-color: gold;
      border-color: black;
      color: black;
    }


    #go-up {
      color: black;
      display: block;
      padding: .8em;
      position: fixed;
      bottom: 50px;
      right: 50px;
      border-color: black;
      text-decoration: none;
      transition: all .3s ease-out;
      background-color: papayawhip;
      border-radius: 50%;
    }

    #go-up:before {
      content: '▲';
      font-size: .9em;
      margin-left: -.7em;
      border: solid .13em black;
      border-radius: 10em;
      width: 1.4em;
      height: 1.4em;
      line-height: 1.2em;
      border-color: inherit;
      display: block;
      text-align: center;
      margin: 0 auto;
    }

    #go-up:hover {
      color: pink;
      border-color: pink;
      bottom: 60px;
      transform: scale(.9)
    }
  </style>

  <script>
    data = "{\"crossword\":[[\"j\",\"i\",\"t\",\"o\",\"m\",\"a\",\"t\",\"e\",\"r\",\"o\"],[\"e\",\"j\",\"i\",\"d\",\"a\",\"t\",\"a\",\"r\",\"i\",\"a\"],[\"m\",\"a\",\"t\",\"a\",\"b\",\"u\",\"r\",\"r\",\"o\",\"s\"],[\"a\",\"r\",\"o\",\"0\",\"o\",\"n\",\"d\",\"a\",\"0\",\"i\"],[\"l\",\"0\",\"0\",\"l\",\"l\",\"0\",\"o\",\"0\",\"e\",\"s\"],[\"0\",\"m\",\"a\",\"l\",\"o\",\"n\",\"0\",\"n\",\"i\",\"0\"],[\"z\",\"a\",\"f\",\"a\",\"0\",\"o\",\"v\",\"e\",\"r\",\"a\"],[\"e\",\"n\",\"t\",\"r\",\"e\",\"v\",\"e\",\"r\",\"a\",\"r\"],[\"n\",\"e\",\"a\",\"0\",\"j\",\"e\",\"t\",\"o\",\"0\",\"r\"],[\"0\",\"s\",\"0\",\"c\",\"e\",\"s\",\"a\",\"n\",\"t\",\"e\"]],\"definitions\":{\"hor\":{\"1\":\"<span class=\\\"abbr\\\" data-title=\\\"adjetivo\\\">adj.</span> <span class=\\\"abbr\\\" data-title=\\\"México\\\">Méx.</span> Perteneciente o relativo al jitomate.\",\"2\":\"<span class=\\\"abbr\\\" data-title=\\\"femenino nombre&nbsp;femenino\\\">f.</span> <span class=\\\"abbr\\\" data-title=\\\"México\\\">Méx.</span> Propietario o usufructuario de un ejido.\",\"3\":\"<span class=\\\"abbr\\\" data-title=\\\"masculino nombre&nbsp;masculino\\\">m.</span> <span class=\\\"abbr\\\" data-title=\\\"Uruguay\\\">Ur.</span> Hueco en el suelo cubierto por barras paralelas en la entrada de una finca, que impide el paso del ganado pero permite el de las personas y vehículos.\",\"4\":\"<span class=\\\"abbr\\\" data-title=\\\"masculino nombre&nbsp;masculino\\\">m.</span> <span class=\\\"abbr\\\" data-title=\\\"Bolivia\\\">Bol.</span>, <span class=\\\"abbr\\\" data-title=\\\"Ecuador\\\">Ec.</span>, <span class=\\\"abbr\\\" data-title=\\\"República&nbsp;Dominicana\\\">R. Dom.</span> y <span class=\\\"abbr\\\" data-title=\\\"Venezuela\\\">Ven.</span> Anillo de compromiso.  //  <span class=\\\"abbr\\\" data-title=\\\"femenino nombre&nbsp;femenino\\\">f.</span> <span class=\\\"abbr\\\" data-title=\\\"El&nbsp;Salvador\\\">El Salv.</span> Idea obsesiva.  //  <span class=\\\"abbr\\\" data-title=\\\"femenino nombre&nbsp;femenino\\\">f.</span> <span class=\\\"abbr\\\" data-title=\\\"filosofía\\\">Fil.</span> En la lógica escolástica, letra que representa la proposición particular afirmativa.\",\"5\":\"<span class=\\\"abbr\\\" data-title=\\\"femenino nombre&nbsp;femenino\\\">f.</span> En la numeración romana, cincuenta.  //  <span class=\\\"abbr\\\" data-title=\\\"femenino nombre&nbsp;femenino\\\">f.</span> Sonido que representa el dígrafo ll.  //  <span class=\\\"abbr\\\" data-title=\\\"conjunción\\\">conj.</span> <span class=\\\"abbr\\\" data-title=\\\"disyuntivo\\\">disyunt.</span> Denota equivalencia, significando 'o sea, o lo que es lo mismo'.  //  <span class=\\\"abbr\\\" data-title=\\\"prefijo\\\">pref.</span> Denota separación.\",\"6\":\"<span class=\\\"abbr\\\" data-title=\\\"masculino nombre&nbsp;masculino\\\">m.</span> <span class=\\\"abbr\\\" data-title=\\\"Argentina\\\">Arg.</span>, Chile, <span class=\\\"abbr\\\" data-title=\\\"Paraguay\\\">Par.</span> y <span class=\\\"abbr\\\" data-title=\\\"Uruguay\\\">Ur.</span> Irrupción o ataque inesperado de indígenas.  //  <span class=\\\"abbr\\\" data-title=\\\"masculino nombre&nbsp;masculino\\\">m.</span> <span class=\\\"abbr\\\" data-title=\\\"Argentina\\\">Arg.</span>, Chile, <span class=\\\"abbr\\\" data-title=\\\"Paraguay\\\">Par.</span> y <span class=\\\"abbr\\\" data-title=\\\"Uruguay\\\">Ur.</span> Irrupción o ataque inesperado de indígenas.\",\"7\":\"<span class=\\\"abbr\\\" data-title=\\\"adverbio adverbial\\\">adv.</span> <span class=\\\"abbr\\\" data-title=\\\"Costa&nbsp;Rica\\\">C. Rica</span>. Salvo, excepto.  //  <span class=\\\"abbr\\\" data-title=\\\"adjetivo\\\">adj.</span> <span class=\\\"abbr\\\" data-title=\\\"Argentina\\\">Arg.</span>, Chile, Cuba, <span class=\\\"abbr\\\" data-title=\\\"Guatemala\\\">Guat.</span>, <span class=\\\"abbr\\\" data-title=\\\"Nicaragua\\\">Nic.</span>, <span class=\\\"abbr\\\" data-title=\\\"Paraguay\\\">Par.</span>, Perú, <span class=\\\"abbr\\\" data-title=\\\"Uruguay\\\">Ur.</span> y <span class=\\\"abbr\\\" data-title=\\\"Venezuela\\\">Ven.</span> pío.\",\"8\":\"<span class=\\\"abbr\\\" data-title=\\\"pronominal verbo&nbsp;pronominal\\\">prnl.</span> <span class=\\\"abbr\\\" data-title=\\\"Argentina\\\">Arg.</span>, <span class=\\\"abbr\\\" data-title=\\\"Bolivia\\\">Bol.</span>, Perú y <span class=\\\"abbr\\\" data-title=\\\"Uruguay\\\">Ur.</span> Dicho de personas, de animales o de cosas: Mezclarse desordenadamente.\",\"9\":\"<span class=\\\"abbr\\\" data-title=\\\"adjetivo\\\">adj.</span> neocatólico. Apl. a <span class=\\\"abbr\\\" data-title=\\\"persona\\\">pers.</span>, <span class=\\\"abbr\\\" data-title=\\\"usado&nbsp;también\\\">u. t.</span> <span class=\\\"abbr\\\" data-title=\\\"como\\\">c.</span> s.  //  <span class=\\\"abbr\\\" data-title=\\\"masculino nombre&nbsp;masculino\\\">m.</span> <span class=\\\"abbr\\\" data-title=\\\"Honduras\\\">Hond.</span> simaruba.  //  <span class=\\\"abbr\\\" data-title=\\\"femenino nombre&nbsp;femenino\\\">f.</span> Decimonovena letra del abecedario español, que puede representar el fonema consonántico vibrante simple, <span class=\\\"abbr\\\" data-title=\\\"por&nbsp;ejemplo\\\">p. ej.</span> en brazo y cara, o el fonema consonántico vibrante múltiple, <span class=\\\"abbr\\\" data-title=\\\"por&nbsp;ejemplo\\\">p. ej.</span> en rojo e israelí.\",\"10\":\"<span class=\\\"abbr\\\" data-title=\\\"femenino nombre&nbsp;femenino\\\">f.</span> Sonido que representa la letra s.  //  <span class=\\\"abbr\\\" data-title=\\\"femenino nombre&nbsp;femenino\\\">f.</span> Sonido que representa la letra s.\"},\"ver\":{\"1\":\"<span class=\\\"abbr\\\" data-title=\\\"adjetivo\\\">adj.</span> Que tiene la distancia y longitud del jeme.  //  <span class=\\\"abbr\\\" data-title=\\\"masculino nombre&nbsp;masculino\\\">m.</span> Escuela budista que tiende a alcanzar la iluminación espiritual mediante la meditación que no se somete al conocimiento intelectual y a sus conceptos. <span class=\\\"abbr\\\" data-title=\\\"Usado&nbsp;también\\\">U. t.</span> <span class=\\\"abbr\\\" data-title=\\\"como\\\">c.</span> adj.\",\"2\":\"<span class=\\\"abbr\\\" data-title=\\\"masculino nombre&nbsp;masculino\\\">m.</span> Ijada del ser humano y de otros mamíferos.  //  <span class=\\\"abbr\\\" data-title=\\\"masculino nombre&nbsp;masculino\\\">m.</span> <span class=\\\"abbr\\\" data-title=\\\"plural\\\">pl.</span> Dioses infernales o almas de los difuntos, considerados benévolos, a los que rendían culto los antiguos romanos.\",\"3\":\"<span class=\\\"abbr\\\" data-title=\\\"masculino nombre&nbsp;masculino\\\">m.</span> almorta.  //  <span class=\\\"abbr\\\" data-title=\\\"femenino nombre&nbsp;femenino\\\">f.</span> <span class=\\\"abbr\\\" data-title=\\\"medicina\\\">Med.</span> Úlcera pequeña, blanquecina, que se forma, durante el curso de ciertas enfermedades, en la mucosa de la boca o de otras partes del tubo digestivo, o en la mucosa genital.\",\"4\":\"<span class=\\\"abbr\\\" data-title=\\\"femenino nombre&nbsp;femenino\\\">f.</span> <span class=\\\"abbr\\\" data-title=\\\"teoría&nbsp;literaria\\\">T. lit.</span> Composición lírica en estrofas de tono elevado, que generalmente ensalza algo o a alguien.  //  <span class=\\\"abbr\\\" data-title=\\\"femenino nombre&nbsp;femenino\\\">f.</span> Cadena de hierro, pendiente en el cañón de la chimenea, con un garabato en el extremo inferior para colgar la caldera, y a poca distancia otro para subirla o bajarla. <span class=\\\"abbr\\\" data-title=\\\"Usado\\\">U.</span> <span class=\\\"abbr\\\" data-title=\\\"masculino nombre&nbsp;masculino\\\">m.</span> en pl.  //  <span class=\\\"abbr\\\" data-title=\\\"femenino nombre&nbsp;femenino\\\">f.</span> En la numeración romana, cien.\",\"5\":\"<span class=\\\"abbr\\\" data-title=\\\"masculino nombre&nbsp;masculino\\\">m.</span> Árbol de Filipinas, de la familia de las ebenáceas, que crece hasta diez u once metros de altura, con flores dioicas, unas solitarias axilares y otras terminales en espiga, hojas alternas y fruto muy semejante al melocotón, pero de carne dura y desabrida.  //  <span class=\\\"abbr\\\" data-title=\\\"masculino nombre&nbsp;masculino\\\">m.</span> Sostén principal de una empresa.\",\"6\":\"<span class=\\\"abbr\\\" data-title=\\\"masculino nombre&nbsp;masculino\\\">m.</span> <span class=\\\"abbr\\\" data-title=\\\"coloquial\\\">coloq.</span> Hombre ignorante y rudo.  //  <span class=\\\"abbr\\\" data-title=\\\"verbal\\\">verb.</span> 2ª <span class=\\\"abbr\\\" data-title=\\\"persona\\\">pers.</span> <span class=\\\"abbr\\\" data-title=\\\"singular\\\">sing.</span> <span class=\\\"abbr\\\" data-title=\\\"presente\\\">pres.</span> de <span class=\\\"abbr\\\" data-title=\\\"subjuntivo\\\">subj.</span> del verbo novar.\",\"7\":\"<span class=\\\"abbr\\\" data-title=\\\"adjetivo\\\">adj.</span> <span class=\\\"abbr\\\" data-title=\\\"tauromaquia\\\">Taurom.</span> Dicho de un toro: Que retrasa su acometida.  //  <span class=\\\"abbr\\\" data-title=\\\"femenino nombre&nbsp;femenino\\\">f.</span> <span class=\\\"abbr\\\" data-title=\\\"Ecuador\\\">Ec.</span> beta (‖ correa).\",\"8\":\"<span class=\\\"abbr\\\" data-title=\\\"verbal\\\">verb.</span> <span class=\\\"abbr\\\" data-title=\\\"imperativo\\\">imper.</span> <span class=\\\"abbr\\\" data-title=\\\"singular\\\">sing.</span> del verbo errar.  //  <span class=\\\"abbr\\\" data-title=\\\"masculino nombre&nbsp;masculino\\\">m.</span> Hombre muy cruel.\",\"9\":\"<span class=\\\"abbr\\\" data-title=\\\"verbal\\\">verb.</span> Forma personal del verbo reír.  //  <span class=\\\"abbr\\\" data-title=\\\"masculino nombre&nbsp;masculino\\\">m.</span> <span class=\\\"abbr\\\" data-title=\\\"Argentina\\\">Arg.</span> Animal carnívoro de la familia de los mustélidos, que alcanza poco más de un metro de longitud, con patas relativamente largas y pelaje corto, liso y de color pardo oscuro, que se alimenta de pequeños mamíferos y de miel.  //  <span class=\\\"abbr\\\" data-title=\\\"femenino nombre&nbsp;femenino\\\">f.</span> Sonido que representa la letra t.\",\"10\":\"<span class=\\\"abbr\\\" data-title=\\\"masculino nombre&nbsp;masculino\\\">m.</span> Tregua, descanso, refugio en las penalidades o contratiempos de la vida.  //  <span class=\\\"abbr\\\" data-title=\\\"interjección interjectivo\\\">interj.</span> <span class=\\\"abbr\\\" data-title=\\\"Usado\\\">U.</span> para denotar que se desaprueba o rechaza algo.\"}},\"file\":\"crucigrama_america_1666880710\",\"title\":\"Crucigrama América\"}";
    active_links = function () {
      document.getElementById("go-up").addEventListener("click", function (e) {
        cross = document.getElementById("crossword-board")
        window.scrollTo({ top: window.scrollY + (cross.getBoundingClientRect().y - 50), behavoir: 'smooth' })
      })

      for (elem of document.getElementsByClassName("num_link")) {
        elem.addEventListener("click", function (e) {
          id = e.currentTarget.getAttribute("id")
          def = document.getElementById(id.replace("num", "def"))
          window.scrollTo({ top: window.scrollY + def.getBoundingClientRect().y, behavior: 'smooth' })
        })
      }

      for (elem of document.getElementsByClassName("cell-input")) {
        elem.addEventListener("click", function (e) {
          e.currentTarget.select()
        })
        elem.addEventListener("blur", function (e) {
          arr_id = e.currentTarget.getAttribute("id").split("-");
          value = e.currentTarget.value;
          if (add_value_to_crossword(arr_id[1], arr_id[2], value) && is_full_board()) {
            if (check_end()) {
              delete_cookie_board(data.file);
              set_resp("Has completado el crucigrama");
            } else {
              set_resp("Hay palabras incorrectas");
            }
          }
        })
      }

      document.getElementById("btn-correct").addEventListener("click", function (e) {
        for (i = 0; i < board.length; i++) {
          for (j = 0; j < board[i].length; j++) {
            cell = board[i][j];
            if (cell != "" && !/\*/.test(cell) && cell != crossword[i][j]) {
              board[i][j] = "*" + cell;
            }
          }
        }
        load_board(board);
        active_links();

        set_resp("Ahí tienes una ayuda");
      })
    }

    add_value_to_crossword = function (id_line, id_col, value) {

      input = document.getElementById("c-" + id_line + '-' + id_col);
      prev_val = document.getElementById("c-" + id_line + '-' + id_col).getAttribute("value");

      if (/[a-zñ]/i.test(value) && prev_val != value) {
        board[id_line][id_col] = value.toUpperCase();
        set_cookie_board(data.file, board);
        input.value = value.toUpperCase();
        input.classList.remove("corrected");
        set_resp("Has colocado una letra");
        return true;
      } else {
        if (value == "") {
          input.classList.remove("corrected");
        }
        set_resp();
        input.innerHTML = "";
        return false
      }
    }

    check_end = function () {
      end = true;
      for (i = 0; i < board.length; i++) {
        for (j = 0; j < board[i].length; j++) {
          if (board[i][j] != crossword[i][j]) { end = false; }
        }
      }
      return end;
    }

    delete_cookie_board = function(name) {
      document.cookie = name + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;Samesite=none;Secure=true;";
    }

    get_blank_crossword = function () {
      blank = []
      for (x = 0; x < 11; x++) {
        aux = [];
        for (y = 0; y < 11; y++) { aux.push(""); }
        blank.push(aux);
      }
      return blank;
    }

    load_board = function (board) {
      if (typeof board == "undefined") {
        board = get_blank_crossword()
      }

      html = ['<div class="board">']

      html.push('<div class="line">')
      for (num = 0; num <= board.length; num++) {
        if (num == 0) {
          html.push('<div class="cell blank"></div>');
        } else {
          html.push('<div id="num-ver-' + num + '" class="cell numv num_link" phx-hook="CrosswordNumToDef"><span>' + num + '</span></div>')
        }
      }
      html.push("</div>")

      for (i = 0; i < board.length; i++) {
        html.push('<div class="line">')
        html.push('<div  id="num-hor-' + (i + 1) + '" class="cell numh num_link" phx-hook="CrosswordNumToDef"><span>' + (i + 1) + '</span></div>')
        for (j = 0; j < board[i].length; j++) {
          var id_i = "c-" + i + "-" + j
          cell = board[i][j]
          html.push('<div class="cell" id="cell-' + i + '-' + j + '">')
          if (/\-/.test(cell)) {
            html.push('<input id="' + id_i + '" class="cell-input" maxlength="1" phx-blur="crossword-blur" value="">')
          } else if (/\*/.test(cell)) {
            html.push('<input id="' + id_i + '" class="cell-input corrected" maxlength="1" phx-blur="crossword-blur" value="' + cell.replace("*", "") + '">')
          } else if (cell === "0") {
            html.push('<span class="block">' + cell + '</span>')
          } else {
            html.push('<input id="' + id_i + '" class="cell-input" maxlength="1" phx-blur="crossword-blur" value="' + cell + '">')
          }
          html.push("</div>")
        }
        html.push("</div>")
      }
      html.push('</div>')

      document.getElementById("crossword-board").innerHTML = html.join("")
    }

    load_defs = function (definitions) {
      html = ['<div class="definitions">']
      for (ori in definitions) {
        html.push('<div class="orientacion">')
        html.push('<h3>' + (ori == "hor" ? "Horizontal" : "Vertical") + '</h3>')
        html.push('<p>')
        for (num in definitions[ori]) {
          html.push('<span id="def-' + ori + '-' + num + '">' + num + '. - </span><span>' + definitions[ori][num] + '</span><br>')
          // html.push('<span id="def-' + ori + '-' + num + '">' + num + '. - </span><span>' + definitions[ori][num].join(" // ") + '</span><br>')
        }
        html.push('</p></div>')
      }
      html.push('</div>')
      document.getElementById("crossword-defs").innerHTML = html.join("")
    }

    init_crossword = function () {
      data = JSON.parse(data);
      crossword = data.crossword.map(line => {
        return line.map(let => { return let.toUpperCase(); });
      });

      prev = get_cookie_board(data.file);
      board = false;
      if(prev){
        if (confirm("Se ha detectado una partida previa. ¿Deseas continuarla?")){
          board = prev;
        } else {
          delete_cookie_board(data.file);
        }
      }
      if(!board){
        board = crossword.map(line => {
          return line.map(let => { return let == 0 ? let : ""; });
        });
      }

      load_board(board);
      load_defs(data.definitions);

      document.getElementById("crossword-title").innerHTML = data.title;
      active_links();
      set_resp("Crucigrama preparado");
    }

    is_full_board = function () {
      for(line of board){
        for(letra of line){
          if (letra == "" || letra == "-1") { return false; }
        }
      }
      return true;
    }

    get_cookie_board = function(name) {
      let cookies = document.cookie;
      for (cookie of document.cookie.split("; ")){
        a_cookie = cookie.split("=");
        if (a_cookie[0] == name && a_cookie[1] != ""){ 
          return JSON.parse(a_cookie[1]);
        }
      }
      return false;
    }

    set_cookie_board = function (cname, board, exdays = 1) {
      board_str = JSON.stringify(board);
      const d = new Date();
      d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));

      document.cookie = cname + "=" + board_str + ";expires=" + d.toUTCString() + ";path=/;Samesite=none;Secure=true;";
    }

    set_resp = function (resp) {
      text = "";
      if (typeof resp != "undefined" && resp != "") {
        text = "<h3>" + resp + "</h3>";
      }
      document.getElementById("crossword-resp").innerHTML = text;
    }

    window.onload = function () {
      init_crossword();

    }

  </script>
</head>

<body>
  <div id="crossword-view">
    <h2 id="crossword-title">Crossword</h2>
    <div id="crossword-resp"></div>
    <div id="crossword-board" class="crossword-board"></div>
    <div class="crossword_controles">
      <button id="btn-correct">Correct crossword</button>
    </div>
    <div id="crossword-defs" class="crossword-defs"></div>
    <div id="go-up" class="go-up" phx-hook="CrosswordGoUp">Go up</div>
  </div>
</body>

</html>
{{< /rawhtml >}}
