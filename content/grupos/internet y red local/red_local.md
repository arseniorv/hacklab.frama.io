+++
title = "Red local"
description = "Notas sobre el proyecto de montar la red local en la Ferro."
date = 2022-09-04T16:07:44+02:00
weight = 10
chapter = false
pre = "<b> </b>"
tags = [
]
toc = true
+++

## Decisiones

### Tareas

- La conexion local podria utilizar la instalacion ya hecha pero se requiere identificar los cables (estan cortados), una crimpadora, cabezales rj45 y routers que sean compatibles con OpenWrt.

- Localizar zonas donde se necesite Internet para saber cuando APs wifi necesitamos.

- Empezar por la biblioteca.

- El local está cableado, con cables que terminan en la sala que hay junto a los baños de la biblioteca (y puede que otros terminen en otro cuarto pequeño que tiene un cuadro eléctrico). Los cables no están etiquetados y en las puntas faltan conectores RJ45.(llegan el lunes 13 y el 15 los traigo)

- Hay que decidir los segmentos de red que necesitamos: p. ej, usuarias del CSO, servicios internos, servicios públicos... Empezar por un segmento para usuarias.

- Material necesario:

[x] Switch.

[x] Cable de corriente para el switch (uno estándar de fuente de alimentación de PC).

[ ] Cables de red, uno por cada sala en la que queramos poner un Access Point. Quizá sean necesarios más en el sitio donde vayamos a poner el switch.

[x] Crimpadora para poner conectores RJ45 en la punta de los cables que hay en el edificio.

[ ] Conectores RJ45 para esos cables, uno por sala.

[ ] APs en los que podamos instalar OpenWRT (se ha comentado la posibilidad de hacer una WiFi Mesh: https://openwrt.org/docs/guide-user/network/wifi/mesh/80211s), uno por sala:

[x] Comtrend 723306-033.

[ ] Otro (editar).

[ ] Otro (editar).

[ ] Transformadores de alimentación para los AP:

[ ] 12v, 1A para el Comtrend 723306-033.

[ ] Portátil, Raspi o lo que sea, si queremos meter todo el tráfico por Tor / VPN / yggdrasil.



