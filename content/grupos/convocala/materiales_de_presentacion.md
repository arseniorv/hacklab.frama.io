
+++
title = "Materiales de presentación"
description = "Cómo transmitir el proyecto a los colectivos"
date = 2023-06-01T16:07:44+02:00
weight = 30
chapter = false
tags = [
]
toc = true
+++

Dependiendo del contexto en el que estemos para presentar el proyecto de mad.convoca.la a los colectivos hay diferentes formatos que podemos usar:

- [Mensaje de difusión general](#mensaje-de-difusión-general)
- [Mensaje de contacto con colectivos cuando subimos un evento suyo](#mensaje-de-contacto-con-colectivos-cuando-subimos-un-evento-suyo)
- [Presentación del colectivo](#presentación-del-colectivo)
- [Presentación en un punto de asamblea](#presentación-en-un-punto-de-asamblea)
- [Presentación completa del proyecto](#presentación-completa-del-proyecto)


## Mensaje de difusión general

Si estás en el grupo del hacklab puedes reenviar [este mensaje](https://t.me/c/1622360513/43/1292) si no puedes copiar y pegar [el texto de la página principal del proyecto](https://hacklab.frama.io/grupos/convocala/mad.convoca.la/).

## Mensaje de contacto con colectivos cuando subimos un evento suyo

Para encontrar la lista de colectivos con los que hemos contactado visita [este pad](https://pad.riseup.net/p/convocala-contacto-colectivos-keep), ahí también puedes encontrar la versión más actualizada del mensaje a enviar.

```
Hola @NombreColectivo, 

Acabamos de subir vuestro evento a https://mad.convoca.la. Esperamos que no os importe 😜, si no es así, comentanoslo y lo borramos :). Podéis verlo aquí: 
    
<<Link al evento>>

Decidnos si véis alguna mejora en los datos del evento.

Somos el colectivo mad.convoca.la (http://mad.convoca.la/) y estamos impulsando una agenda política y contracultural en Madrid. Velamos por la okupación digital de los espacios de comunicación y autoorganización. De forma que intentamos dar apoyo a la difusión comunitaria a los eventos de los movimientos sociales.

Si queréis contribuir, podéis empezar a subir vuestros eventos o difundir la web. El boca a boca siempre es la mejor estrategia. También os invitamos a nuestra asamblea itinerante y abierta a colectivos. La próxima es el Martes 6 de Junio en La Villana.

Para saber más sobre el proyecto, aquí tenéis más información: https://hacklab.frama.io/grupos/convocala y https://mad.convoca.la/about.

No dudéis en comentarnos cualquier duda o mejora que os surja,
Un abrazo
```

```
[Opcional]
También hemos visto que tenéis un calendario en la web con Wordpress. Existe una extensión para Wordpress que os permitiría subir los eventos automáticamente a mad.convoca.la. Si os interesa podemos ayudaros a instalarlo.
```

## Presentación en un punto de asamblea 

Útil para cuando presentamos el proyecto en la asamblea del propio colectivo a contactar, o como introducción si hay gente nueva en la asamblea de mad.convoca.la. La idea es que no lleve más de 10 minutos

### Presentación del proyecto

Somos un colectivo que está creando una **agenda política y cultural autónoma**, que quiere contribuir a **difundir y coordinar acciones** públicas de colectivos sociales y políticos en **Madrid**. Para sacar adelante el proyecto usamos dos herramientas [una página web](#presentación-de-la-pagina) y [el colectivo](#presentación-del-colectivo).

### Presentación de la página

En [la página web](https://mad.convoca.la) se pueden ver y subir los eventos de forma amigable e intuitiva. Al estar hecha específicamente para este fin tiene funcionalidades muy interesantes como:

- Automatización de difusión: La página tiene mecanismos para propagar la información de manera automática cuando se suba o edite un evento. Es capaz de publicar eventos en:

  - [Canales de telegram](https://t.me/madconvocala).
  - [La página del colectivo](https://hacklab.frama.io/agenda/).
  - [El calendario de las activistas](https://mad.convoca.la/feed/ics?show_recurrent=true).
  - [En redes sociales libres](https://mastodon.social/@madrid@mad.convoca.la).
  - [En lectores de RSS](https://mad.convoca.la/export).

- Facilidad de creación de eventos: A través de:

  - [La propia web](https://mad.convoca.la/add)
  - El plugin de Wordpress 
  - El bot de telegram. 

  Tienes las siguientes ayudas a la hora de subir un evento:

  - Autocompletado de tags y lugares.
  - Aviso sobre eventos ya convocados para evitar pisarnos.
  - Selectores gráficos de fecha y hora.
  - Selección de fragmento de imagen a mostrar.

- Facilidad de visualización de eventos: A través de:

  - Página principal con resumen de todos los eventos.
  - Filtrado de por barrios, lugares, tags o fechas.
  - Página específica de cada evento 
  - Ver en un mapa dónde va a ser el evento.
  - Creando rutas andando, en bici o coche para saber cómo llegar.

- Creación anónima de eventos: Por si no quieres crearte cuenta o que se te asocie con el evento.

- Recuperar la soberanía comunicativa: La página está alojada en servidores radicales ([Sin Dominio](https://sindominio.net/)), utilizamos una herramienta de código libre y colaborativa ([Gancio](https://gancio.org/)) y un colectivo asambleario ([Convócala Madrid](https://hacklab.frama.io/grupos/convocala/mad.convoca.la/)) decide qué se publica y qué no. De manera que no dependemos de las redes y filtros capitalistas para difundir nuestros eventos.

### Presentación del colectivo

Desde el colectivo promotor hemos establecido una serie de [principios](https://hacklab.frama.io/grupos/convocala/carta_valores/#principios) que los eventos han de cumplir para ser admitidos. No obstante en el día a día de la gestión de la agenda surgen debates y decisiones políticas que no sentimos que tengamos la potestad de tomar solas. Por ejemplo:

- Qué colectivos no están representados y queremos que estén.
- Si subir o no un evento que está en los grises de los principios y normas que determinan cuales son aptos.
- Actualización de dichos principios y normas.
- Moderar eventos de luchas en las que no estamos presentes.

Por ello queremos que el colectivo crezca hasta representar la diversidad de las luchas de esta ciudad y que entre todas tomemos estas decisiones.

Para ello os invitamos a la siguiente asamblea itinerante del colectivo que va a ser el `<< introducir día >>` en `<< introducir lugar >>`. 

## Presentación completa del proyecto

Útil para una kafeta o una sesión específica para dar a conocer el proyecto.
