+++
author = "HackLab"
title = "HackLab Unipoular"
description = "HackLab Uni Popular en La Ferroviaria"
weight = 1
draft = false
date = 2022-05-28T16:07:44+02:00
+++

# HackLab Uni Popular

![Logo](logos/logo_150x150.jpg?height=150&classes=shadow)

## 👥 ¿Quiénes somos? 👥

Un grupo de personas que nos juntamos para formar un espacio interdisciplinar de aprendizaje cooperativo y cacharreo tecnológico utilizando software libre.

{{% button href="https://t.me/+EmWc6-CNLPJlMTc0" style="transparent" icon="fab fa-telegram" icon-position="right" %}}**¡Únete al supergrupo en telegram y pregunta tus dudas!**{{% /button %}}

## 📅 ¿Cómo encontrarnos? 📌

Todos los miércoles de 18h a 21h en la Ferroviaria (Plaza Luca de Tena, 7)

{{< hacklab-events >}}

## Tabla de Contenidos

{{% children description="true" depth="4" /%}}

## Documentos importantes

* [Hoja de asistencia a la asamblea general de la
    Ferro](https://cryptpad.disroot.org/sheet/#/2/sheet/edit/I0UE-oF9nrl4Na4L7xRd32FE/)
* [Listado de intereses de la gente del
    hacklab](https://cryptpad.disroot.org/sheet/#/2/sheet/edit/CbxTFYsc9OLBVRUGoUy-HBqn/).