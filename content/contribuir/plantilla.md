+++
title = "Plantilla para crear nuevas entradas con el mismo formato"
description = "Ejemplo de la descripción."
date = 2022-05-28T16:07:44+02:00
weight = 40
chapter = false
pre = "<b> </b>"
tags = [

]
toc = true
+++

Lo más importante para utilizar esta plantilla es copiar los metadatos de arriba (todo lo escrito entre los dos separadores +++). Lo demás se puede borrar.
