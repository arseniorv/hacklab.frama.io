+++
title = "Editar con git"
description = "Pasos para editar con git."
date = 2022-05-28T16:07:44+02:00
weight = 25
chapter = false
pre = "<b> </b>"
tags = [
    "Web estática",
    "Hugo",
    "Git"
]
toc = true
+++

Para seguir estos pasos se requiere conocimientos básicos del lenguaje de marcado Markdown y una cuenta en la instancia de GitLab donde se aloja la página web. Además, se requieren conocimientos básicos de git, aunque si no los tienes, este puede ser un buen momento para aprender a utilizar este sistema de gestión de versiones.

### Editar un contenido o entrada ya creada

Pasos:

1. Clonar el [repositorio de la web](https://framagit.org/hacklab/hacklab.frama.io). Se puede clonar mediante SSH, si previamente se ha subido la clave pública a framagit, o mediante HTTPS. Por seguridad, es recomendable realizarlo mediante SSH, debido a que la opción de HTTPS pide autenticarse con usuario y contraseña cada vez que se quiera realizar algún cambio. En cambio, mediante SSH y `ssh-add`, sólo será necesario introducir una vez la contraseña SSH y la autenticación es más fuerte al hacerse por claves público-privadas.    

```bash
# SSH
git clone git@framagit.org:hacklab/hacklab.frama.io.git

# HTTPS
https://framagit.org/hacklab/hacklab.frama.io.git
``` 

2. Actualizar el submódulo que contine el tema de Hugo. Al clonar el repositorio, la carpeta del módulo se descargará vacía. 

```
git submodule update --init
```

3. [Instalar Hugo](https://gohugo.io/getting-started/installing/)

    Si estás en Debian o Ubuntu, se recomienda instalar una versión > 0.104.1. Puedes seleccionar el fichero hugo_**extended**_XXX_linux-XXX.deb desde [releases](https://github.com/gohugoio/hugo/releases). Una vez descargado dirígete a la carpeta de Descargas y corre:

{{% notice style="warning" %}}
Es muy importante que se instale la versión **extended**, sino el tema elegido no funcionará.
{{% /notice %}}

    ```bash
    sudo dpkg -i nombredelpaquete.deb
    ```
    
    Para comprobar que la versión instalada es la correcta corre:
    
    ```bash
    hugo version
    ```

    Si usas Mac asumiendo que tienes [Homebrew](https://brew.sh/):

    ```bash
    brew install hugo
    ```

    Si estás con Windows y usas [Chocolatey](https://chocolatey.org/):

    ```
    choco install hugo -confirm
    ```


1. Servir la web con `hugo serve` y abrir [http://localhost:1313]() en el navegador.

2. Lo ideal es utilizar los [issues](https://framagit.org/hacklab/hacklab.frama.io/-/issues) y [merge requests](https://framagit.org/hacklab/hacklab.frama.io/-/merge_requests) para indicar qué cambio vamos a realizar en el código. Aunque al principio puede parecer un poco lioso, son buenas prácticas para la revisión de código y adminitración de tareas. En caso de abrir un issue con un merge request asociado, utilizarempos `git checkout -b feat/nueva-rama` para editar sobre ella. Abriremos el proyecto con el editor de código preferido y modificaremos un archivo en la carpeta `content/`.

3. Visualizar los cambios en el navegador después de guardar el fichero modificado. Comprobar que los cambios funcionan en local.

4. Después de comprobar que todo funciona, si hemos creado un merge request asociado al issue que queremos solucionar, haremos un commit en la rama creada con `git commit -m "Mensaje descriptivo de los cambios"` y subirlo al repositorio origen con `git push origin feat/nueva-rama`. En este caso, se habrá creado un merge request que habrá que aprobar para que se fusionen los cambios de la `feat/nueva-rama` con la `main`. Si no hemos creado un merge request asociado, simplemente haremos un commit en la rama main y lo suberemos al repositorio de origen.

5. Esperar 30 segundos o un minuto a que termine de completarse la integración y despliegue contínuo (CI/CD) y refrescar la página web con "Ctrl + Shift + R" para recargar la página sin utilizar la caché del navegador. Si todo ha ido correctamente, se podrán visualizar los nuevos cambios.

### Crear una entrada nueva en la web

Previamente, es necesario completar los pasos de la sección anterior hasta el paso 4, incluido.

Pasos:

1. Abrir el proyecto con el editor de código preferido y con una terminal. Si queremos crear un nuevo capítulo o sección con el nombre `nuevo`, ejecutamos lo siguiente:

    ```zsh
    hugo new --kind chapter nuevo/_index.md
    ```

    Si simplemente queremos crear una nueva entrada, escribimos:

    ```zsh
    hugo new hugo/quick_start.md
    ```

2. Después, ya se podrá visualizar los cambios en el navegador después de guardar el fichero modificado.

3. Hacer un commit en la rama main y subirlo a la instancia de GitLab.

4. Esperar 30 segundos o un minuto a que termine de completarse la integración y despliegue contínuo (CI/CD) y refrescar la página web con "Ctrl + Shift + R" para recargar la página sin utilizar la caché del navegador. Si todo ha ido correctamente, se podrán visualizar los nuevos cambios.
