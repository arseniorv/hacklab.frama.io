+++
title = "Charlas y talleres"
description = "Charlas y talleres del HackLab"
date = 2022-09-04T16:07:44+02:00
weight = 15
chapter = false
pre = "<b><i class='fas fa-hat-wizard'></i> </b>"
tags = [
]
toc = true
+++

## Encuentro: Privacidad, Autodefensa y Soberanía Digital

Formato: foro, charlas y fiesta.

Fecha: Sábado 17 de Junio del 2023

Hora: 18:00

En este encuentro de privacidad, autodefensa y soberania digital tendremos unas actividades donde podremos compartir entre todas las necesidades y dudas que tenemos sobre estos principios. Además de pasarlo bien, intentaremos aprender y plantear una base sólida sobre la que comenzar. 
Os esperamos :)

{{< figure src="/images/encuentro_privacidad.jpg" >}}

## Mesa redonda: Software Libre en el móvil

Formato: mesa redonda

Fecha: Miércoles 25 de Enero del 2023

Hora: 19:00

Mesa redonda para debatir el uso de software libre en el móvil. Se utiliza un [pad de apuntes conjuntos](https://pad.riseup.net/p/movil-software-libre-keep).

{{< figure src="/images/software_libre_movil.png" >}}

## Analiza datos de Twitter desde cero

Formato: taller

Fecha: Miércoles 16 de Noviembre del 2022

Hora: 19:30

Ponente: Atenea

Taller práctico sobre análisis de datos en Twitter.

Los requisitos para seguir el taller desde una máquina GNU/Linux es tener instalado Gephi y Python, o utilizar una máquina virtual en Oracle VM Virtual Box utilizando las siguientes [instrucciones](https://www.dropbox.com/s/j0p26bmgmct3vll/como_instalar_VM_taller_datos_twitter.pdf?dl=0).

{{< figure src="/images/taller_análisis_de_redes_de_twitter.png" >}}

## Taller de laboratorio de Pentesting

Formato: taller

Fecha: Miércoles 5 de Octubre del 2022

Hora: 18:30

Ponente: ReD

{{< figure src="/images/taller_de_laboratorio_de_Pentesting.jpg" >}}

## Amenazas en Linux y cómo defenderse

Formato: charla

Fecha: Viernes 22 de Junio del 2022

Hora: 18:30

Ponente: Alien

{{< figure src="/images/charla_amenazas_linux.jpg" >}}


